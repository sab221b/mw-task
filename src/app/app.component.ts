import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Cards',
      url: '/mw-task/cards',
      icon: 'mail'
    },
    {
      title: 'Outbox',
      url: '/mw-task/outbox',
      icon: 'paper-plane'
    },
    {
      title: 'Favorites',
      url: '/mw-task/Favorites',
      icon: 'heart'
    },
    {
      title: 'Archived',
      url: '/mw-task/Archived',
      icon: 'archive'
    },
    {
      title: 'Trash',
      url: '/mw-task/Trash',
      icon: 'trash'
    },
    {
      title: 'Spam',
      url: '/mw-task/Spam',
      icon: 'warning'
    }
  ];
  // public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('mw-task/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }
}

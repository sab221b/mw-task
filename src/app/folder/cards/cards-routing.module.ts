import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CardsPage } from './cards.page';
import { BrandingComponent } from './branding/branding.component';
import { DesignComponent } from './design/design.component';
import { ContentComponent } from './content/content.component';


const routes: Routes = [
  {
    path: '',
    component: CardsPage,
    children: [
      {
        path: 'branding',
        component: BrandingComponent
      },
      {
        path: 'design',
        component: DesignComponent
      },
      {
        path: 'content',
        component: ContentComponent
      },
      {
        path: '',
        redirectTo: 'branding',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CardsRoutingModule { }

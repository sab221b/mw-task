import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardsRoutingModule } from './cards-routing.module';
import { BrandingComponent } from './branding/branding.component';
import { DesignComponent } from './design/design.component';
import { ContentComponent } from './content/content.component';


@NgModule({
  declarations: [
    DesignComponent,
    ContentComponent,
    BrandingComponent,
  ],
  imports: [
    CommonModule,
    CardsRoutingModule
  ]
})
export class CardsModule { }
